import 'package:flutter/material.dart';
import '../person_info.dart';



class OutputScreen extends StatefulWidget {
  final Person person;

  OutputScreen({Key? key, required this.person}) : super(key: key);

  @override
  State<OutputScreen> createState() => _OutputScreenState();
}

class _OutputScreenState extends State<OutputScreen> {
  Map info = {};

  @override
  Widget build(BuildContext context) {
    // info = info.isNotEmpty ? info : ModalRoute.of(context)!.settings.arguments as Map;
    //
    // String name = info['name'];
    // String birth = info['birth'];


    void backPressed() {
      Navigator.pop(context);
    }

    return WillPopScope(
      onWillPop: () async {
         backPressed();
         return false;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_outlined),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: const Text('User Information'),
          backgroundColor: Colors.indigoAccent,
        ),
        body: Center(
          child: Column(
            children:  [
              const Padding(
                padding: EdgeInsets.only(bottom: 70, top: 40),
                child: Text(
                  'Thank You!!!',
                  style: TextStyle(
                    color: Colors.orangeAccent,
                    fontSize: 35
                  ),
                ),
              ),
              SizedBox(
                height: 70,
                width: 320,
                child: TextField(
                  textAlign: TextAlign.center,
                  decoration: const InputDecoration(
                    labelText: 'Your Name:',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red)),
                  ),
                  controller: TextEditingController()..text = widget.person.name,
                  enabled: false,
                  style: const TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 40),
                child: SizedBox(
                  height: 70,
                  width: 320,
                  child: TextField(
                    textAlign: TextAlign.center,
                    decoration: const InputDecoration(
                      labelText: 'Birth Date:',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.red,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red)),
                    ),
                    controller: TextEditingController()..text = widget.person.birthDate,
                    enabled: false,
                    style: const TextStyle(
                      fontSize: 20
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
