import 'package:assignment/person_info.dart';
import 'package:assignment/screens/output_screen.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class InputScreen extends StatefulWidget {
  const InputScreen({Key? key}) : super(key: key);

  @override
  State<InputScreen> createState() => _InputScreenState();
}

class _InputScreenState extends State<InputScreen> {
  late final TextEditingController userName;
  late final TextEditingController dob;
  bool enableButton = false;
  // final String userPostfix = '12 June,2022';


  @override
  void initState() {
    userName = TextEditingController();
    dob = TextEditingController();
    super.initState();

    userName.addListener(() {
      final enableButton = dob.text.isNotEmpty && userName.text.isNotEmpty;

      setState(() => this.enableButton = enableButton);
    });
    dob.addListener(() {
      final enableButton = dob.text.isNotEmpty && userName.text.isNotEmpty;

      setState(() => this.enableButton = enableButton);
    });
  }

  @override
  void dispose() {
    userName.dispose();
    dob.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('User Input Page'),
        backgroundColor: Colors.indigoAccent,
      ),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 40),
              child: SizedBox(
                height: 45,
                width: 300,
                child: TextField(
                  controller: userName,
                  keyboardType: TextInputType.name,
                  autocorrect: false,
                  decoration: const InputDecoration(
                    labelText: 'Name',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 45,
              width: 300,
              child: TextField(
                controller: dob,
                // onChanged: (value) {
                //   value.endsWith(userPostfix)
                //       ? dob.text = '12 June,2022'
                //       : dob.text = '12 June,2022';
                // },
                onTap: () async {
                  DateTime? selectDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1970),
                      lastDate: DateTime.now()
                  );
                  if(selectDate != null) {
                    setState(() {
                      dob.text = DateFormat('dd MMM, yyyy').format(selectDate);
                    });
                  }
                },
                decoration: const InputDecoration(
                  labelText: 'Date of Birth',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.green)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30),
              child: ElevatedButton(
                style: TextButton.styleFrom(
                    onSurface: Colors.white,
                    backgroundColor: Colors.deepPurple),
                onPressed: enableButton
                    ? () {
                        Person info = Person(userName.text, dob.text);

                        Navigator.push(context, MaterialPageRoute(builder: (context) => OutputScreen(person: info)));
                      }
                    : null,
                child: const Text('Submit'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
