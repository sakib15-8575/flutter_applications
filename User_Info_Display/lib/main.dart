import 'package:assignment/loading/load_page.dart';
import 'package:assignment/screens/input_screen.dart';
import 'package:assignment/screens/output_screen.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(
    MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => LoadingScreen(),
        '/home': (context) => InputScreen(),
        // '/info': (context) => OutputScreen(birth: null,),
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    ),
  );
}

